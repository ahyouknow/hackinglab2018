#!/usr/bin/env python3
from scapy.all import *
from random import choice
from time import sleep

def main():
    flag="cd928aa4b419469a907622c7c93b0e13"
    slices=[slice(0,8), slice(8, 16), slice(16,24), slice(24,32)]
    while 1:
        flagpiece=choice(slices)
        string="flag"+str(slices.index(flagpiece))+":"+flag[flagpiece]
        ips=IP(dst="192.168.1.100/24", src="127.0.0.1")/TCP(dport=[80,443])/string
        packet=choice([p for p in ips])
        send(packet)
        sleep(5)

if __name__ == '__main__':
    main()
