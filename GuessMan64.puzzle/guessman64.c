#include <stdio.h>
#include <stdlib.h>

void main(){
  int secret = -32698;
  int guess;
  printf("Guess the random number stored at %p and I will give you the flag\n", &secret);
  scanf("%i", &guess);
  if (guess == secret){
    printf("1d516549dea0f60432b96206160ced01\n");
  } else {
    printf("WRONG\n");
  }
}
